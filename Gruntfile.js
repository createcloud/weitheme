module.exports = function(grunt) {
    require('time-grunt')(grunt); //Grunt处理任务进度条提示

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        less: {
            development: {
                options: {
                    // paths: ['less/zhidao']
                },
                files: {
                    'assets/style.css': ['src/less/*.less']
                }
            }
        },
        concat: {
            angular: {
                src: [
                    'bower_components/angular/angular.min.js',
                    'bower_components/angular-ui-router/release/angular-ui-router.min.js',
                    'bower_components/angular-animate/angular-animate.min.js',
                    'bower_components/angular-sanitize/angular-sanitize.min.js',
                    'bower_components/angular-local-storage/angular-local-storage.min.js'
                ],
                dest: 'assets/vendor/angular.pack.js'
            }
        },
        copy: {
            css: {
                expand: true,
                flatten: true,
                src: ['bower_components/mobile-angular-ui/dist/css/mobile-angular-ui-base.min.css'],
                dest: 'assets'
            },
            js: {
                expand: true,
                flatten: true,
                src: [
                    'bower_components/mobile-angular-ui/dist/js/mobile-angular-ui.min.js'
                ],
                dest: 'assets/vendor'
            }
        },
        watch: {
            options: {
                //开启 livereload
                livereload: true,
                //显示日志
                dateFormate: function(time) {
                    grunt.log.writeln('编译完成,用时' + time + 'ms ' + (new Date()).toString());
                    grunt.log.writeln('Wating for more changes...');
                }
            },
            less: {
                files: ['src/less/*.less', 'src/less/*/*.less'],
                tasks: ['less']
            }
        }
    });

    grunt.event.on('watch', function(action, filepath, target) {
        grunt.log.writeln(target + ': ' + '文件: ' + filepath + ' 变动状态: ' + action);
    });

    grunt.registerTask('build', ['concat', 'copy']);
    grunt.registerTask('default', ['less']);
    grunt.loadNpmTasks("grunt-contrib-less");
    grunt.loadNpmTasks("grunt-contrib-concat");
    grunt.loadNpmTasks("grunt-contrib-copy");
    grunt.loadNpmTasks("grunt-contrib-watch");
}